import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'propertyName' })

export class PropertyFilterPipe implements PipeTransform {
  transform(properties: any, searchText: any): any {
    if(searchText == null) return properties;

    return properties.filter(function(property){
      return property.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
    })
  }
}
