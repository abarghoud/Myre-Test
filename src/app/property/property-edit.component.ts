import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms'
import {IProperty} from "./property";
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import {PropertyService} from "./property.service";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-property-edit',
  templateUrl: './property-edit.component.html',
  styleUrls: ['./property-edit.component.css']
})

export class PropertyEditComponent implements OnInit {
  form: FormGroup;
  property: IProperty = {
    name: '',
    accessibility: null,
    id: 0,
    surface: null,
    description: '',
    energyClass: '',
    imageUrl: ''
  };
  propertyId: number;

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private propertySvc: PropertyService,
              private confirmationService: ConfirmationService,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.propertyId = params['id'];
      if (this.propertyId) {
        this.getPropertyById(this.propertyId);
      }
      this.createForm();
    });
  }

  createForm() {
    this.form = this.fb.group({
      name: [this.property.name, Validators.required],
      accessibility: [this.property.accessibility, Validators.required],
      surface: [this.property.surface, Validators.required],
      description: [this.property.description, Validators.required],
      energyClass: [this.property.energyClass, Validators.required],
      imageUrl: [this.property.imageUrl, Validators.compose([Validators.required, Validators.pattern('(https?:\\/\\/.*\\.(?:png|jpg))')])],
      id: [this.property.id]
    })
  }

  getPropertyById(id: number) {
    this.propertySvc.getProperty(id)
      .subscribe((property: IProperty) => {
          this.property = property;
          this.createForm();
        },
        (err) => console.error(err))
  }

  cancel() {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?, You may lose your data.',
      accept: () => {
        this.router.navigateByUrl('/properties')
      }
    });
  }

  saveProperty() {
    this.form.value.name = this.form.value.name.toUpperCase();
    this.propertySvc.saveProperty(this.form.value).subscribe((res) => {
        this.router.navigateByUrl('/properties')
      },
      (err) => console.error(err))
  }


}
