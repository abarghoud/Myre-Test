import {Component, OnInit} from '@angular/core';
import {PropertyService} from "./property.service";
import {IProperty} from "./property";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-property-details',
  templateUrl: './property-details.component.html',
  styleUrls: ['./property-details.component.css']
})
export class PropertyDetailsComponent implements OnInit {
  isPropertyLoaded: boolean = false;
  property: IProperty;
  propertyId: number;
  constructor(private propertySvc: PropertyService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.propertyId = params['id'];
      this.getPropertyById(this.propertyId);
    });
  }

  getPropertyById(id: number) {
    this.propertySvc.getProperty(id)
      .subscribe((property: IProperty) => {
          this.property = property;
          this.isPropertyLoaded = true;
        },
        (err) => console.error(err))
  }

}
