import { Component, OnInit } from '@angular/core';
import {PropertyService} from "./property.service";
import {IProperty} from "./property";
import {PropertyFilterPipe} from "./property-filter.pipe";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-property-list',
  templateUrl: './property-list.component.html',
  styleUrls: ['./property-list.component.css'],
})
export class PropertyListComponent implements OnInit {
  isPropertiesLoaded: boolean = false;
  searchText: string;
  properties: IProperty[];
  constructor(private propertySvc: PropertyService, private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.getProperties();
  }

  getProperties() {
    this.propertySvc.getProperties().subscribe((properties: IProperty[]) => {
      this.properties = properties;
      this.isPropertiesLoaded = true;
    }, (err) => {console.error(err)})
  }


  deleteProperty(id: number) {
    this.confirmationService.confirm({
      message: `Are you sure that you want to perform this action?, You won't be able to restore this data.`,
      accept: () => {
        this.propertySvc.deleteProperty(id).subscribe(
          (deleted) => {
            this.getProperties();
          },
          (err) => console.error(err)
        )
      }
    });

  }

}
