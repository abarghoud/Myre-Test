import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RatingModule} from 'primeng/components/rating/rating';
import { PropertyRoutingModule } from './property-routing.module';
import { PropertyListComponent } from './property-list.component';
import { PropertyEditComponent } from './property-edit.component';
import { PropertyDetailsComponent } from './property-details.component';
import {ConfirmDialogModule} from 'primeng/components/confirmdialog/confirmdialog';
import { PropertyService } from './property.service';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PropertyFilterPipe} from "./property-filter.pipe";
import {ConfirmationService} from "primeng/primeng";
import {AuthGuardService} from "../user/authGuard.service";

@NgModule({
  imports: [
    CommonModule,
    PropertyRoutingModule,
    RatingModule,
    FormsModule,
    ReactiveFormsModule,
    ConfirmDialogModule
  ],
  declarations: [
    PropertyListComponent,
    PropertyEditComponent,
    PropertyDetailsComponent,
    PropertyFilterPipe
  ],
  providers: [PropertyService, ConfirmationService, AuthGuardService]
})
export class PropertyModule { }
