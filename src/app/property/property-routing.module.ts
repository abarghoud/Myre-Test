import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PropertyListComponent } from './property-list.component';
import {PropertyEditComponent} from "./property-edit.component";
import {PropertyDetailsComponent} from "./property-details.component";
import {AuthGuardService} from "../user/authGuard.service";

const routes: Routes = [
  {path: 'properties', canActivate: [AuthGuardService], component: PropertyListComponent},
  {path: 'property', canActivate: [AuthGuardService], component: PropertyEditComponent},
  {path: 'property/:id', canActivate: [AuthGuardService], component: PropertyEditComponent},
  {path: 'property-details/:id', canActivate: [AuthGuardService], component: PropertyDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PropertyRoutingModule { }
