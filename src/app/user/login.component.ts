import {Component, OnInit} from '@angular/core';
import {IUser} from "./user";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: IUser = {userName: '', isAdmin: null, id: null};

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  isLoggedIn(){
    if (this.getCookie('user') && this.getCookie('user').length > 0){
      return true;
    }
    return false;
  }

  login() {
    var date = new Date();
    date.setTime(date.getTime()+(120*1000));
    var expires = date.toUTCString();

    document.cookie = `user=${this.user.userName};expires=${expires}; path=/`;
    this.router.navigateByUrl('/properties')
  }

  private getCookie(name: string) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
      begin = dc.indexOf(prefix);
      if (begin != 0) return null;
    }
    else
    {
      begin += 2;
      var end = document.cookie.indexOf(";", begin);
      if (end == -1) {
        end = dc.length;
      }
    }
    // because unescape has been deprecated, replaced with decodeURI
    //return unescape(dc.substring(begin + prefix.length, end));
    return decodeURI(dc.substring(begin + prefix.length, end));
  }

}
