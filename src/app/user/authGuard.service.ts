import {CanActivate, Router} from "@angular/router";
import {Injectable} from "@angular/core";

@Injectable()
export class AuthGuardService implements CanActivate {
  canActivate(): boolean {
    return this.isUserAuthenticated();
  }

  constructor(private router: Router){}

  isUserAuthenticated(): boolean {
    if (this.getCookie('user') && this.getCookie('user').length > 0){
      return true;
    }
    this.router.navigateByUrl('/login');
    return false;
  }



  private getCookie(name: string) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
      begin = dc.indexOf(prefix);
      if (begin != 0) return null;
    }
    else
    {
      begin += 2;
      var end = document.cookie.indexOf(";", begin);
      if (end == -1) {
        end = dc.length;
      }
    }
    // because unescape has been deprecated, replaced with decodeURI
    //return unescape(dc.substring(begin + prefix.length, end));
    return decodeURI(dc.substring(begin + prefix.length, end));
  }

}
